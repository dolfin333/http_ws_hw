import { addClass, removeClass } from "./domHelper.mjs";

const username = sessionStorage.getItem("username");
let numOfLetters = 0;
let enteredNumOfLetters = 0;

if (!username) {
  window.location.replace("/login");
}

document.getElementById("title_username").innerHTML = username;
const socket = io("", { query: { username } });

document.getElementById("quit-results-btn").addEventListener("click", () => {
  addClass(document.getElementById("modal"), "display-none");
  removeClass(document.getElementById("quit-room-btn"), "display-none");
  removeClass(document.getElementById("ready-btn"), "display-none");
})

document.getElementById("add-room-btn").addEventListener("click", () => {
  const room_name = prompt("Enter room name");
  if (room_name) {
    socket.emit('createRoom', room_name);
  }
});

document.getElementById("quit-room-btn").addEventListener("click", () => {
  socket.emit('leaveRoom');
  removeClass(document.getElementById('rooms-page'), 'display-none');
  addClass(document.getElementById('game-page'), 'display-none');
});

const ReadyButton = document.getElementById("ready-btn");
ReadyButton.addEventListener("click", () => {
  if (ReadyButton.innerHTML === 'Ready') {
    ReadyButton.innerHTML = 'Not Ready'
  }
  else {
    ReadyButton.innerHTML = 'Ready'
  }
  socket.emit('Ready');
});

function eventJoinRoomBtn(e) {
  socket.emit('joinRoom', e.target.getAttribute('room_id'));
}

function MakeUserStr(user) {
  const status = user.isReady ? "green" : "red";
  let isYou = "";
  if (user.username === username) {
    isYou = "(you)"
  }
  return `
      <div id="user-${user.username}">
      <div class="user_info">
      <div class="ready-status-${status}" id="ready-status-${user.username}"></div>
      ${user.username} ${isYou}
      </div>
      <div class="bar">
      <div class="user-progress ${user.username}"></div>
      </div>
      </div>`
}

function UpdateRoom(user, isAdd) {
  const usersElement = document.getElementById("users");
  let str = "";
  if (isAdd) {
    str += MakeUserStr(user);
    usersElement.innerHTML += str;
  }
  else {
    usersElement.removeChild(document.getElementById(`user-${user.username}`))
  }
}

function AddSpanToEveryLetter(text) {
  const letterArr = text.split("");
  let newText = "";
  letterArr.forEach((letter, index) => {
    let isNext = ""
    if (index === 0) {
      isNext = "next";
    }
    newText += `<span class="not-entered ${isNext}">${letter}</span>`
  })
  numOfLetters = letterArr.length;
  return newText;
}


function onKeyDown(e) {
  if (e.keyCode == 32) {
    e.preventDefault();
  }
  const letterElements = document.getElementsByClassName('not-entered');
  if (letterElements[0]) {
    if (e.key == letterElements[0].innerHTML) {
      if (letterElements[1]) {
        addClass(letterElements[1], "next");
      }
      addClass(letterElements[0], "entered");
      removeClass(letterElements[0], "next");
      removeClass(letterElements[0], "not-entered");
      ++enteredNumOfLetters;
      const bar_width = (100 * enteredNumOfLetters) / numOfLetters;
      socket.emit("Change progress bar", bar_width, username);
    }
  }
}

function getWinnersStr(arr, is100) {
  let str = ""
  if (is100) {
    arr.sort(function (a, b) {
      return new Date(a.time) - new Date(b.time);
    });
    arr.forEach((user, index) => {
      const ready_status = document.getElementById(`ready-status-${user.username}`);
      addClass(ready_status, 'ready-status-red');
      removeClass(ready_status, 'ready-status-green');
      let isYou = ""
      if (user.username === username) {
        isYou += " (you)";
      }
      str += `<div id="place-${index + 1}">${index + 1}. ${user.username} ${isYou}#${index + 1}</div>`
    })
  }
  else {
    let map = arr.reduce((r, i) => {
      r[i.bar_width] = r[i.bar_width] || [];
      r[i.bar_width].push(i);
      return r;
    }, {});
    arr = [];
    for (let key in map) {
      arr.push(map[key]);
    }
    arr.sort(function (a, b) {
      return b[0].bar_width - a[0].bar_width;
    });
    arr.forEach((users, index) => {
      let usernames = ""
      users.forEach((user, index) => {
        const ready_status = document.getElementById(`ready-status-${user.username}`);
        addClass(ready_status, 'ready-status-red');
        removeClass(ready_status, 'ready-status-green');
        usernames += user.username;
        if (user.username === username) {
          usernames += " (you)"
        }
        if (users.length !== index + 1) {
          usernames += ", ";
        }
      })
      str += `<div id="place-${index + 1}">${index + 1}. ${usernames}#${index + 1}</div>`
    })
  }
  return str;
}

socket.on('AddRooms', rooms_name => {
  let src = "";
  for (const key in rooms_name) {
    let style = ""
    if (!rooms_name[key].isShow) {
      style = 'style="display: none"'
    }
    src += `
    <div class="room" id="${key}" ${style}>
    <div class="users_connected">
    <div id="count_users_${key}">${rooms_name[key].usersCount}</div><span> users connected</span>
    </div>
    <div class="room_name">${key}</div>
    <button class="join-room-btn" room_id="${key}">Join</button>
    </div>
    `;
  }
  document.getElementById('rooms').innerHTML += src;
  document.querySelectorAll(".join-room-btn").forEach(element => {
    element.addEventListener("click", eventJoinRoomBtn)
  })
})

socket.on('UserExists', () => {
  window.alert("User with this name is already exists");
  sessionStorage.removeItem("username");
  window.location.replace("/login");
})

socket.on('RoomExists', () => {
  window.alert("Room with this name is already exists");
})

socket.on('RoomCreated', room_name => {
  document.querySelectorAll(".join-room-btn").forEach(element => {
    element.removeEventListener("click", eventJoinRoomBtn)
  })
  document.getElementById('rooms').innerHTML += `
  <div class="room" id="${room_name}">
  <div class="users_connected">
  <div id="count_users_${room_name}">1</div><span> users connected</span>
  </div>
  <div class="room_name">${room_name}</div>
  <button class="join-room-btn" room_id="${room_name}">Join</button>
  </div>
  `;
  document.querySelectorAll(".join-room-btn").forEach(element => {
    element.addEventListener("click", eventJoinRoomBtn)
  })
})

socket.on('JoinRoom', (users, roomId) => {
  addClass(document.getElementById('rooms-page'), 'display-none');
  removeClass(document.getElementById('game-page'), 'display-none');
  document.getElementById("game-page-room-name").innerHTML = roomId;
  const usersElement = document.getElementById("users");
  let str = "";
  users.forEach(user => {
    str += MakeUserStr(user);
  });
  usersElement.innerHTML += str;
}
)

socket.on("ClearRoom", () => {
  document.getElementById("users").innerHTML = "";
})

socket.on('UpdateRoom', (user, isAdd) => {
  UpdateRoom(user, isAdd);
})

socket.on('ChangeUsersAmount', (room_name, user_amount) => {
  document.getElementById(`count_users_${room_name}`).innerHTML = user_amount;
})

socket.on('DeleteRoom', room_name => {
  document.getElementById('rooms').removeChild(document.getElementById(room_name));
})


socket.on('UserReady', (username, isReady) => {
  const ready_status = document.getElementById(`ready-status-${username}`);
  if (isReady) {
    removeClass(ready_status, 'ready-status-red');
    addClass(ready_status, 'ready-status-green');
  }
  else {
    addClass(ready_status, 'ready-status-red');
    removeClass(ready_status, 'ready-status-green');
  }
})

socket.on('HideRoom', room_name => {

  document.getElementById(room_name).style.display = 'none';
})

socket.on('ShowRoom', room_name => {
  document.getElementById(room_name).style.display = 'grid';
})

socket.on('Start timer', () => {
  document.getElementById("ready-btn").innerHTML = "Ready";
  addClass(document.getElementById("ready-btn"), 'display-none');
  addClass(document.getElementById("quit-room-btn"), 'display-none');
  removeClass(document.getElementById("timer"), 'display-none');
  socket.emit("Timer");
})

socket.on('Timer', newValue => {
  document.getElementById("timer").innerHTML = newValue;
})

socket.on('Game timer', newValue => {
  document.getElementById("game-timer").innerHTML = newValue;
})

socket.on('Start game', async randId => {
  numOfLetters = 0;
  enteredNumOfLetters = 0;
  addClass(document.getElementById("timer"), 'display-none');
  removeClass(document.getElementById("text-container"), 'display-none');
  document.getElementById("left-time").style.display = "flex";
  const randText = await (await fetch(`/game/texts/${randId}`)).text();
  document.getElementById("text-container").innerHTML = AddSpanToEveryLetter(randText);
  document.addEventListener('keydown', onKeyDown);
})

socket.on("Change progress bar", (bar_width, username) => {
  const bar = document.querySelector(`.user-progress.${username}`);
  if (bar_width >= 100) {
    bar.style.background = 'green';
    bar.style.width = '100%';
  }
  else {
    bar.style.width = `${bar_width}%`;
  }
})

socket.on("Game end", (winners) => {
  let arr = [];
  let str = "";
  for (const key in winners) {
    arr.push({ username: key, ...winners[key] })
  }
  if (arr.every(user => user.bar_width === 100)) {
    str = getWinnersStr(arr, true);
  }
  else {
    str = getWinnersStr(arr, false);
  }
  removeClass(document.getElementById("modal"), "display-none");
  document.getElementById("winners").innerHTML = str;
  document.getElementById("left-time").style.display = "none";
  addClass(document.getElementById("text-container"), 'display-none');
  document.removeEventListener('keydown', onKeyDown);
  document.querySelectorAll(`.user-progress`).forEach(bar => {
    bar.style.width = 0;
  });
  document.getElementById("game-timer").innerHTML = "";
  document.getElementById("timer").innerHTML = "";
  socket.emit("StopGameTimer");
})
