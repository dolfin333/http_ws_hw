import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from "./config";
import { texts } from "../data";
const usernames = [];
const activeRoomnames = {};
const roomsTextIds = {};
const rooms = {};
const winners = {};
const currentRooms = {};

function EveryoneIsReady(users) {
    return users.every(function (user) {
        return user.isReady;
    });
}

const getRoomEvents = (io, socket) => {
    const username = socket.handshake.query.username;
    let currentRoom = null;
    let isRoomShow = false;
    let isReady = false;
    let isGameOn = false;
    let gameTimer;
    let timer;

    if (usernames.indexOf(username) !== -1) {
        usernames.push(username);
        socket.emit('UserExists');
    }
    else {
        usernames.push(username);
        socket.emit('AddRooms', activeRoomnames)
    }

    function HideRoom() {
        activeRoomnames[currentRoom].isShow = false;
        io.sockets.emit("HideRoom", currentRoom);
        isRoomShow = false;
    }

    function ShowRoom() {
        activeRoomnames[currentRoom].isShow = true;
        io.sockets.emit("ShowRoom", currentRoom);
        isRoomShow = true;
    }

    function ifEveryoneIsReady() {
        if (currentRoom) {
            roomsTextIds[currentRoom] = Math.floor(Math.random() * (texts.length))
            rooms[currentRoom] = rooms[currentRoom].map(user => {
                user.isReady = false;
                return user;
            });
            isReady = false;
            isGameOn = true;
            io.to(currentRoom).emit("Start timer");
            HideRoom();
        }
    }

    function GameEnd() {
        io.to(currentRoom).emit("StopGameTimer")
        rooms[currentRoom] = rooms[currentRoom].map(user => {
            user.isEnd = false;
            return user
        });
        isGameOn = false;
        if (rooms[currentRoom].length !== MAXIMUM_USERS_FOR_ONE_ROOM) {
            ShowRoom()
        }
        io.to(currentRoom).emit("Game end", winners[currentRoom]);
    }

    // The function startStreaming starts streaming data to all the users
    function startGameTimer() {
        let counter = SECONDS_FOR_GAME;
        socket.emit('Game timer', counter);
        gameTimer = setInterval(function () {
            counter--;
            socket.emit('Game timer', counter);
            if (counter === 0) {
                clearInterval(gameTimer);
                GameEnd();
            }
        }, 1000);
    }

    function stopGameTimer() {
        clearInterval(gameTimer);
    }

    function stopTimer() {
        clearInterval(timer);
    }

    function LeaveRoom() {
        rooms[currentRoom] = rooms[currentRoom].filter(user => user.username !== username);
        activeRoomnames[currentRoom].usersCount = activeRoomnames[currentRoom].usersCount - 1;
        if (winners[currentRoom]) {
            delete winners[currentRoom][username];
        }
        if (rooms[currentRoom].length === 0) {
            delete activeRoomnames[currentRoom]
            delete rooms[currentRoom];
            if (winners[currentRoom]) {
                delete winners[currentRoom];
            }
            io.sockets.emit("DeleteRoom", currentRoom);
        }
        else {
            socket.to(currentRoom).emit("UpdateRoom", { username: username, isReady: isReady }, false);
            io.sockets.emit("ChangeUsersAmount", currentRoom, rooms[currentRoom].length);
            if (rooms[currentRoom].every(user => user.isEnd === true)) {
                GameEnd();
            }
            if (!isGameOn) {
                ShowRoom();
            }
            if (EveryoneIsReady(rooms[currentRoom])) {
                ifEveryoneIsReady();
            }
        }
        socket.emit("ClearRoom");
        socket.leave(currentRoom);
        currentRoom = null;
        isGameOn = false;
        isReady = false;
        currentRooms[username] = currentRoom;
        stopGameTimer();
        stopTimer();
    }
    return {
        stopGameTimer,
        LeaveRoom,
        createRoom: (roomId) => {
            if (rooms[roomId]) {
                socket.emit("RoomExists");
            } else {
                socket.join(roomId);
                currentRoom = roomId;
                currentRooms[username] = currentRoom;
                activeRoomnames[roomId] = { usersCount: 1, isShow: true };
                rooms[roomId] = [{ username: username, isReady: isReady, isEnd: false }];
                socket.emit("JoinRoom", rooms[roomId], roomId);
                io.sockets.emit("RoomCreated", roomId);
                isRoomShow = true;
            }
        },
        joinRoom: (roomId) => {
            currentRoom = roomId;
            currentRooms[username] = currentRoom;
            socket.join(roomId);
            const user = { username: username, isReady: isReady, isEnd: false };
            rooms[roomId].push(user);
            activeRoomnames[roomId].usersCount = activeRoomnames[roomId].usersCount + 1;
            socket.emit("JoinRoom", rooms[roomId], roomId);
            socket.to(roomId).emit("UpdateRoom", user, true);
            io.sockets.emit("ChangeUsersAmount", roomId, rooms[roomId].length);
            if (rooms[roomId].length === MAXIMUM_USERS_FOR_ONE_ROOM) {
                HideRoom();
            }
        },
        Ready: () => {
            isReady = !isReady;
            rooms[currentRoom] = rooms[currentRoom].map(user => {
                if (user.username === username) {
                    user.isReady = !user.isReady
                }
                return user
            })
            io.to(currentRoom).emit("UserReady", username, isReady);
            if (EveryoneIsReady(rooms[currentRoom])) {
                ifEveryoneIsReady();
            }
        },

        Timer: () => {
            isReady = false;
            let counter = SECONDS_TIMER_BEFORE_START_GAME;
            io.to(currentRoom).emit('Timer', counter);
            timer = setInterval(function () {
                counter--;
                io.to(currentRoom).emit('Timer', counter);
                if (counter === 0) {
                    io.to(currentRoom).emit('setRandText', roomsTextIds[currentRoom]);
                    winners[currentRoom] = {};
                    rooms[currentRoom].forEach(user => {
                        winners[currentRoom][user.username] = { bar_width: 0, time: 0 };
                    })
                    isGameOn = true;
                    socket.emit("Start game", roomsTextIds[currentRoom]);
                    startGameTimer();
                    clearInterval(timer);
                }
            }, 1000);
        },
        ChangeProgressBar: (bar_width, username) => {
            winners[currentRoom][username] = { bar_width: bar_width, time: new Date };
            io.to(currentRoom).emit('Change progress bar', bar_width, username);
            if (bar_width >= 100) {
                rooms[currentRoom] = rooms[currentRoom].map(user => {
                    if (user.username === username) {
                        user.isEnd = true;
                    }
                    return user
                })
            }
            if (rooms[currentRoom].every(user => user.isEnd === true)) {
                stopGameTimer();
                GameEnd();
            }
        },
        Disconnect: () => {
            if (currentRoom) {
                LeaveRoom(currentRoom);
                stopGameTimer();
                stopTimer();
            }
            usernames.splice(usernames.indexOf(username), 1);
        }
    }
}


export { getRoomEvents };