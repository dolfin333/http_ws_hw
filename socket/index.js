import { getRoomEvents } from "./roomEvents";

export default io => {
  io.on("connection", socket => {
    const roomEvents = getRoomEvents(io, socket);

    socket.on("createRoom", roomEvents.createRoom)

    socket.on("joinRoom", roomEvents.joinRoom)

    socket.on("leaveRoom", roomEvents.LeaveRoom)

    socket.on("Ready", roomEvents.Ready)

    socket.on("StopGameTimer", roomEvents.stopGameTimer)

    socket.on("Timer", roomEvents.Timer)

    socket.on("Change progress bar", roomEvents.ChangeProgressBar)

    socket.on("disconnect", roomEvents.Disconnect);
  });
};
